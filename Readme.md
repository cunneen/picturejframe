#PictureJFrame and PictureJPanel

These are simple extensions of the swing JFrame and JPanel classes.

They allow you to have a picture as the background of your JFrame or JPanel.

![PictureJFrame](https://bitbucket.org/cunneen/picturejframe/raw/master/doc/screenshot.png)
![PictureJPanel](https://bitbucket.org/cunneen/picturejframe/raw/master/doc/picturejpanel.png)

To use them, just:

1. Download the [zip file here](https://bitbucket.org/cunneen/picturejframe/get/master.zip) (packaged as a NetBeans project).
2. Extract and import the files into your project.
3. Extend the PictureJFrame class and then use it as you would a JFrame.
4. Use the PictureJPanel class as you would a JPanel.
5. To set a custom image, provide a URL to the setImageURL(...) method.

###For example (JFrame): 

    :::java
    import biz.cunneen.picturejframe.PictureJFrame;
    public class PictureJFrameDemo extends PictureJFrame {

        /**
         * Creates new form PictureJFrameDemo
         */
        public PictureJFrameDemo() {
            this.setImageURL(PictureJFrameDemo.class.getResource("Jellyfish.jpg"));
            initComponents();
        }
        // ...snip...
    }

###For example (JPanel): 

    :::java
    import biz.cunneen.picturejframe.PictureJPanel;
    public class PictureJPanelDemo extends JFrame {
        private PictureJPanel pictureJPanel;
        /** Creates a new PictureJPanelDemo */
        public PictureJPanelDemo() {
            pictureJPanel = new PictureJPanel();
            // ... add your components to pictureJPanel here ...
            /// ... snip ...
            // set a custom background image for the jpanel
            pictureJPanel.setImageURL(PictureJPanelDemo.class.getResource("Jellyfish.jpg"));
        }
        // ... snip ...
    }

